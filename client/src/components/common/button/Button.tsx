import { ReactNode } from 'react';
import styles from './Button.module.css';

type ButtonProps = {
  onClick: () => void;
  children: ReactNode;
  isLoading?: boolean;
};

const Button = ({ children, onClick, isLoading }: ButtonProps) => {
  return (
    <button className={styles.button} onClick={onClick} disabled={isLoading}>
      {children}
    </button>
  );
};

export default Button;
