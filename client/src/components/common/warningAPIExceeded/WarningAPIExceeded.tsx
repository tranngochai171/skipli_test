import styles from './WarningAPIExceeded.module.css';

const WarningAPIExceeded = () => {
  return (
    <div className={styles.warningText}>
      Data might be not available because of API rate limit exceeded from Github
      API
    </div>
  );
};

export default WarningAPIExceeded;
