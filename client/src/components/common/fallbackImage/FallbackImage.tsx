import React, { useState } from 'react';
import PlaceHolderImage from '../../../assets/images/placeholder.jpg';

interface FallbackImageProps extends React.ImgHTMLAttributes<HTMLImageElement> {
  src: string;
  fallbackSrc?: string;
}

const FallbackImage: React.FC<FallbackImageProps> = ({
  src,
  fallbackSrc = PlaceHolderImage,
  ...props
}) => {
  const [imageSrc, setImageSrc] = useState(src);

  const handleImageError = () => {
    setImageSrc(fallbackSrc);
  };

  return (
    <img
      src={imageSrc}
      onError={handleImageError}
      alt='skipli img'
      {...props}
    />
  );
};

export default FallbackImage;
