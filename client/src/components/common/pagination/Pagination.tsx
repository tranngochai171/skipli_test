import React from 'react';
import styles from './Pagination.module.css';

interface Props {
  totalPages: number;
  page: number;
  handleChangePage: (newPage: number) => void;
}

const BUTTONS_TO_SHOW = 5;

const Pagination: React.FC<Props> = ({
  totalPages,
  page,
  handleChangePage,
}) => {
  const handleClick = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    newPage: number,
  ) => {
    event.preventDefault();
    handleChangePage(newPage);
  };

  const renderPageNumbers = () => {
    let pageNumbers = [];
    let start = 1,
      end = totalPages;
    if (totalPages > BUTTONS_TO_SHOW) {
      if (page <= Math.floor(BUTTONS_TO_SHOW / 2)) {
        start = 1;
        end = BUTTONS_TO_SHOW;
      } else if (page + Math.floor(BUTTONS_TO_SHOW / 2) >= totalPages) {
        start = totalPages - BUTTONS_TO_SHOW + 1;
        end = totalPages;
      } else {
        start = page - Math.floor(BUTTONS_TO_SHOW / 2);
        end = page + Math.floor(BUTTONS_TO_SHOW / 2);
      }
    }
    for (let i = start; i <= end; i++) {
      pageNumbers.push(
        <button
          key={i}
          disabled={i === page}
          onClick={e => handleClick(e, i)}
          className={styles.button}
        >
          {i}
        </button>,
      );
    }
    return pageNumbers;
  };

  return (
    <div className={styles.container}>
      <button
        disabled={page === 1}
        onClick={e => handleClick(e, page - 1)}
        className={styles.button}
      >
        Prev
      </button>
      {renderPageNumbers()}
      <button
        disabled={page === totalPages}
        onClick={e => handleClick(e, page + 1)}
        className={styles.button}
      >
        Next
      </button>
    </div>
  );
};

export default Pagination;
