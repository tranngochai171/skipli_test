import React, { useState } from 'react';
import styles from './Dropdown.module.css';

export type OptionDropdownType = {
  value: string | number;
  label: string | number;
};

interface DropdownProps {
  options: OptionDropdownType[];
  onChange: (value: OptionDropdownType) => void;
  defaultValue?: OptionDropdownType;
}

const Dropdown: React.FC<DropdownProps> = ({
  options,
  onChange,
  defaultValue,
}) => {
  const [selectedValue, setSelectedValue] = useState(
    defaultValue ?? options[0],
  );

  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const option = options.find(
      ({ value }) => value.toString() === event.target.value,
    );
    if (option) {
      setSelectedValue(option);
      onChange(option);
    }
  };

  return (
    <select
      className={styles.dropdown}
      value={selectedValue.value}
      onChange={handleChange}
    >
      {options.map(({ value, label }) => (
        <option key={value} value={value}>
          {label}
        </option>
      ))}
    </select>
  );
};

export default Dropdown;
