import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Outlet } from 'react-router-dom';
import { useLogout } from '../../hooks/useAuth';
import { getIsAuthenticated } from '../../redux/selectors/auth.selector';
import Button from '../common/button/Button';
import styles from './LayoutWithNavbar.module.css';

const LayoutWithNavbar = () => {
  const [sticky, setSticky] = useState(false);
  const isAuthenticated = useSelector(getIsAuthenticated);
  const { mutate: mutateLogout } = useLogout();
  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const handleScroll = () => {
    if (window.pageYOffset > 50) {
      setSticky(true);
    } else {
      setSticky(false);
    }
  };

  const onHandleLogout = () => {
    mutateLogout();
  };

  return (
    <>
      <nav className={`${styles.navbar} ${sticky ? styles.navbar : ''}`}>
        <h1 className='navbar-brand'>SKIPLI TEST</h1>
        {isAuthenticated ? (
          <Button onClick={onHandleLogout}>Logout</Button>
        ) : null}
      </nav>
      <div className={styles.bodyContainer}>
        <Outlet />
      </div>
    </>
  );
};

export default LayoutWithNavbar;
