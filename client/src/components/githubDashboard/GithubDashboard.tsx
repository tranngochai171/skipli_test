import React, { useCallback, useMemo } from 'react';
import { useSelector } from 'react-redux';
import {
  getFavoriteGithubUsers,
  getIsLoadingUserDetail,
} from '../../redux/selectors/auth.selector';
import FallbackImage from '../common/fallbackImage/FallbackImage';
import styles from './GithubDashboad.module.css';
import { FaRegHeart, FaHeart } from 'react-icons/fa';
import { useUpdateFavoriteGithubUsers } from '../../hooks/useGithub';
import Loader from '../common/loader/Loader';

interface Data {
  id: number;
  login: string;
  avatar_url: string;
  html_url: string;
  repos_url: string;
  followers_url: string;
}

interface Props {
  data: Data[];
}

const GithubDashboard: React.FC<Props> = ({ data = [] }) => {
  const favoriteGithubUsers = useSelector(getFavoriteGithubUsers);
  const isLoadingUserDetail = useSelector(getIsLoadingUserDetail);
  const {
    mutate,
    isLoading: isLoadingMutateUpdate,
  } = useUpdateFavoriteGithubUsers();
  const onHandleUpdateFavoriteGithubUser = useCallback(
    (githubId: number) => {
      mutate({ githubId });
    },
    [mutate],
  );
  const isLoading = useMemo(
    () => isLoadingMutateUpdate || isLoadingUserDetail,
    [isLoadingMutateUpdate, isLoadingUserDetail],
  );
  const bodyTableContent = useMemo(() => {
    if (!data?.length) {
      return (
        <tr>
          <td colSpan={6} style={{ textAlign: 'center' }}>
            No Data Available
          </td>
        </tr>
      );
    }
    return data?.map((item: Data) => (
      <tr key={item.id}>
        <td>{item.id}</td>
        <td>
          <div className={styles.loginSection}>
            <FallbackImage
              className={styles.profileIcon}
              src={item.avatar_url}
            />
            {item.login}
          </div>
        </td>
        <td>
          <div className={styles.wrapText}>
            <a href={item.html_url} rel='noreferrer' target='_blank'>
              {item.html_url}
            </a>
          </div>
        </td>
        <td>
          <div className={styles.wrapText}>
            <a href={item.repos_url} rel='noreferrer' target='_blank'>
              {item.repos_url}
            </a>
          </div>
        </td>
        <td>
          <div className={styles.wrapText}>
            <a href={item.followers_url} rel='noreferrer' target='_blank'>
              {item.followers_url}
            </a>
          </div>
        </td>
        <td>
          <div
            className={styles.favoritesSection}
            onClick={() => onHandleUpdateFavoriteGithubUser(item.id)}
          >
            {favoriteGithubUsers.includes(item.id) ? (
              <FaHeart color='#F36967' />
            ) : (
              <FaRegHeart />
            )}
          </div>
        </td>
      </tr>
    ));
  }, [data, favoriteGithubUsers, onHandleUpdateFavoriteGithubUser]);
  return (
    <>
      <div style={{ position: 'fixed', top: 38, right: 200 }}>
        {isLoading ? <Loader /> : null}
      </div>
      <table className={styles.table}>
        <thead>
          <tr>
            <th>ID</th>
            <th>Login</th>
            <th>HTML URL</th>
            <th>Public Repos</th>
            <th>Followers URL</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{bodyTableContent}</tbody>
      </table>
    </>
  );
};

export default GithubDashboard;
