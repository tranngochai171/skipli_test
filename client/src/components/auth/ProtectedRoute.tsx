import { ReactNode } from 'react';
import { useSelector } from 'react-redux';
import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { getIsAuthenticated } from '../../redux/selectors/auth.selector';

type ProtectedRouteProps = {
  redirectPath?: string;
  children?: ReactNode;
};

const ProtectedRoute = ({
  redirectPath = '/',
  children,
}: ProtectedRouteProps): any => {
  const isAuthenticatedVendorUser = useSelector(getIsAuthenticated);
  const location = useLocation();
  if (!isAuthenticatedVendorUser) {
    return <Navigate to={redirectPath} replace state={{ from: location }} />;
  }
  return children ? children : <Outlet />;
};

export default ProtectedRoute;
