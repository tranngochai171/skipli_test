import { RootState } from '../store';

export const getAuthProperties = (state: RootState) => state.auth;

export const getIsAuthenticated = (state: RootState) =>
  state.auth?.isAuthenticated;

export const getPhoneNumber = (state: RootState) => state.auth?.phoneNumber;

export const getFavoriteGithubUsers = (state: RootState) =>
  state.auth?.favoriteGithubUsers;

export const getIsLoadingUserDetail = (state: RootState) =>
  state.auth?.isLoading;
