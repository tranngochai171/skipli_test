import { RootState } from '../store';

export const getGithubUsersFilterProperties = (state: RootState) =>
  state.githubUsers;
