import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axiosUtil from '../../utils/axios.util';

export interface CounterState {
  isAuthenticated: boolean;
  phoneNumber: string;
  favoriteGithubUsers: number[];
  isLoading: boolean;
}

const initialState: CounterState = {
  isAuthenticated: false,
  phoneNumber: '',
  favoriteGithubUsers: [],
  isLoading: false,
};

export const getUserDetail = createAsyncThunk(
  'auth/getUserDetail',
  async (phoneNumber: string) => {
    const response = await axiosUtil.get(
      `/auth/get-detail?phoneNumber=${encodeURIComponent(phoneNumber)}`,
    );
    return response.data;
  },
);

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (
      state,
      action: PayloadAction<{
        phoneNumber: string;
        favoriteGithubUsers: number[];
      }>,
    ) => {
      state.isAuthenticated = true;
      state.phoneNumber = action.payload.phoneNumber;
      state.favoriteGithubUsers = action.payload.favoriteGithubUsers;
    },
    logout: state => {
      state.isAuthenticated = false;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getUserDetail.pending, (state, action) => {
        state.isLoading = true;
      })
      .addCase(getUserDetail.fulfilled, (state, action) => {
        state.isLoading = false;
        state.favoriteGithubUsers = action.payload?.favoriteGithubUsers ?? [];
      })
      .addCase(getUserDetail.rejected, (state, action) => {
        state.isLoading = false;
      });
  },
});

// Action creators are generated for each case reducer function
export const { login, logout } = authSlice.actions;

export default authSlice.reducer;
