import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import commonConstants from '../../constants/common.constant';

export const PROPERTY_KEY = {
  PAGE: 'page',
  PER_PAGE: 'perPage',
  KEYWORD: 'keyword',
};

const initialState = {
  page: commonConstants.GITHUB_USERS_DEFAULT_QUERY_PARAMS.page,
  perPage: commonConstants.GITHUB_USERS_DEFAULT_QUERY_PARAMS.perPage,
  keyword: commonConstants.GITHUB_USERS_DEFAULT_QUERY_PARAMS.keyword,
};

type PayloadProperty = {
  type: string;
  value: string | number | any[];
};

type PayloadProperties = PayloadProperty[];

export const githubUsersSlice = createSlice({
  name: 'githubUsers',
  initialState,
  reducers: {
    setProperty: (state, action: PayloadAction<PayloadProperty>) => {
      // @ts-ignore
      state[action.payload.type] = action.payload.value;
    },
    setProperties: (state, action: PayloadAction<PayloadProperties>) => {
      if (action.payload.length > 0) {
        action.payload.forEach((item: PayloadProperty) => {
          if (state.hasOwnProperty(item.type)) {
            // @ts-ignore
            state[item.type] = item.value;
          }
        });
      }
    },
  },
});

// Action creators are generated for each case reducer function
export const { setProperty, setProperties } = githubUsersSlice.actions;

export default githubUsersSlice.reducer;
