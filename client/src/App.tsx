import store, { persistor } from './redux/store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import ProtectedRoute from './components/auth/ProtectedRoute';
import Login from './pages/login/Login';
import LayoutWithNavbar from './components/layout/LayoutWithNavbar';
import { ToastContainer, toast } from 'react-toastify';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import 'react-toastify/dist/ReactToastify.css';
import {
  MutationCache,
  QueryCache,
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query';
import commonConstants from './constants/common.constant';
import { logout } from './redux/slices/authSlice';
import statusCodeConstant from './constants/statusCode.constant';
import Users from './pages/users/Users';
import UserProfile from './pages/userProfile/UserProfile';

const handleError = (error: any) => {
  // Fallback Error Catch If we don't define onError when using useQuery
  toast.error(
    error?.response?.data?.message ??
      error?.message ??
      commonConstants.SOMETHING_WENT_WRONG,
  );
};

const checkExpiredAndLogout = async (error: any) => {
  // Check Expired Time here to logout;
  if (error.response.status === statusCodeConstant.FORBIDDEN) {
    store.dispatch(logout());
    toast.error(commonConstants.EXPIRED_SESSION_MESSAGE);
  }
};

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
      refetchOnWindowFocus: false,
      onError: handleError,
    },
    mutations: {
      retry: false,
      onError: handleError,
    },
  },
  queryCache: new QueryCache({
    onError: checkExpiredAndLogout,
  }),
  mutationCache: new MutationCache({
    onError: checkExpiredAndLogout,
  }),
});

const router = createBrowserRouter([
  {
    path: '/',
    element: <LayoutWithNavbar />,
    children: [
      { index: true, element: <Login /> },
      {
        element: <ProtectedRoute />,
        children: [
          { path: 'users', element: <Users /> },
          { path: 'user-profile', element: <UserProfile /> },
        ],
      },
    ],
  },
]);

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RouterProvider router={router} />
          <ToastContainer position='top-center' />
        </PersistGate>
      </Provider>
      <ReactQueryDevtools />
    </QueryClientProvider>
  );
}

export default App;
