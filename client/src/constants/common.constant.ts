const commonConstants = {
  DEFAULT_PATH_ROUTE: '/users',
  SOMETHING_WENT_WRONG: 'Something went wrong',
  EXPIRED_SESSION_MESSAGE: 'Your session has expired',
  GITHUB_USERS_DEFAULT_QUERY_PARAMS: {
    page: 1,
    perPage: 10,
    keyword: '',
  },
  /* millisecond */
  STALE_TIME: {
    MIN_1: 1000 * 60,
  },
  PER_PAGE_OPTIONS: [
    {
      label: '10 pages',
      value: 10,
    },
    {
      label: '20 pages',
      value: 20,
    },
    {
      label: '30 pages',
      value: 30,
    },
    {
      label: '50 pages',
      value: 50,
    },
  ],
};

export default commonConstants;
