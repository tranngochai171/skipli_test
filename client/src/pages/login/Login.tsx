import Button from '../../components/common/button/Button';
import { useLogin, useSendSMS } from '../../hooks/useAuth';
import styles from './Login.module.css';
import PhoneInput, {
  isValidPhoneNumber,
  Value,
} from 'react-phone-number-input';
import { useState, useMemo, useEffect } from 'react';
import 'react-phone-number-input/style.css';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import Loader from '../../components/common/loader/Loader';
import { useSelector } from 'react-redux';
import { getIsAuthenticated } from '../../redux/selectors/auth.selector';
import { useNavigate } from 'react-router-dom';
import commonConstants from '../../constants/common.constant';

const validationSchemaStep1 = Yup.object().shape({
  phone: Yup.string()
    .test('is-valid-phone', 'Phone is not valid', (value: any) =>
      !!value ? isValidPhoneNumber(value) : false,
    )
    .required('Required'),
});

const validationSchemaStep2 = Yup.object().shape({
  code: Yup.string().length(6, 'Should be 6 digits').required('Required'),
});

type initialValuesTypeStep1 = {
  phone: string;
};

const initialValuesStep1: initialValuesTypeStep1 = {
  phone: '',
};

type initialValuesTypeStep2 = {
  code: string;
};

const initialValuesStep2: initialValuesTypeStep2 = {
  code: '',
};

const Login = () => {
  const [selectedPhone, setSelectedPhone] = useState('');
  const isAuthenticated = useSelector(getIsAuthenticated);
  const navigate = useNavigate();
  useEffect(() => {
    if (!!isAuthenticated) {
      navigate(commonConstants.DEFAULT_PATH_ROUTE);
    }
  }, [isAuthenticated, navigate]);
  const { mutate: mutateSendSMS, isLoading: isLoadingSendSMS } = useSendSMS();
  const { mutate: mutateLogin, isLoading: isLoadingLogin } = useLogin();
  const isLoading = useMemo(() => isLoadingSendSMS || isLoadingLogin, [
    isLoadingSendSMS,
    isLoadingLogin,
  ]);
  const handleSendSMS = () => {
    if (isValidStep1) {
      mutateSendSMS(
        { phoneNumber: valuesStep1.phone },
        {
          onSuccess: () => {
            setSelectedPhone(valuesStep1.phone);
          },
        },
      );
    }
  };
  const handleResetPhone = () => {
    setSelectedPhone('');
    setFieldValueStep1('phone', '');
  };

  const onHandleVerifyCode = () => {
    if (isValidStep2) {
      mutateLogin({ accessCode: valuesStep2.code, phoneNumber: selectedPhone });
    }
  };
  const {
    values: valuesStep1,
    errors: errorsStep1,
    setFieldValue: setFieldValueStep1,
    handleSubmit: handleSubmitStep1,
    isValid: isValidStep1,
  } = useFormik({
    initialValues: initialValuesStep1,
    onSubmit: handleSendSMS,
    validationSchema: validationSchemaStep1,
  });

  const {
    values: valuesStep2,
    errors: errorsStep2,
    handleChange: handleChangeStep2,
    handleBlur: handleBlurStep2,
    handleSubmit: handleSubmitStep2,
    isValid: isValidStep2,
  } = useFormik({
    initialValues: initialValuesStep2,
    onSubmit: onHandleVerifyCode,
    validationSchema: validationSchemaStep2,
  });

  const onChangePhone = (value: Value) => {
    setFieldValueStep1('phone', value);
  };

  return (
    <div className={styles.container}>
      <div className={styles.itemSection}>
        <div className={styles.itemField}>
          <PhoneInput
            value={valuesStep1.phone}
            onChange={onChangePhone}
            disabled={!!selectedPhone}
          />
          {errorsStep1.phone ? (
            <span className={styles.itemErrorMessage}>{errorsStep1.phone}</span>
          ) : null}
        </div>
        <Button
          onClick={!!selectedPhone ? handleResetPhone : handleSubmitStep1}
          isLoading={isLoading}
        >
          {!!selectedPhone ? 'Reset' : 'Send SMS'}
        </Button>
        {isLoadingSendSMS ? <Loader /> : null}
      </div>
      {selectedPhone ? (
        <div className={styles.itemSection}>
          Code:
          <div className={styles.itemField}>
            <input
              name='code'
              type='text'
              onChange={handleChangeStep2}
              onBlur={handleBlurStep2}
            />
            {errorsStep2.code ? (
              <span className={styles.itemErrorMessage}>
                {errorsStep2.code}
              </span>
            ) : null}
          </div>
          <Button onClick={handleSubmitStep2} isLoading={isLoading}>
            Verify SMS
          </Button>
          {isLoadingLogin ? <Loader /> : null}
        </div>
      ) : null}
    </div>
  );
};

export default Login;
