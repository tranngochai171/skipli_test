import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import FallbackImage from '../../components/common/fallbackImage/FallbackImage';
import WarningAPIExceeded from '../../components/common/warningAPIExceeded/WarningAPIExceeded';
import { useGetFavoriteGithubUsers } from '../../hooks/useGithub';
import { getPhoneNumber } from '../../redux/selectors/auth.selector';
import styles from './UserProfile.module.css';

interface Data {
  id: number;
  login: string;
  avatar_url: string;
  html_url: string;
  public_repos: number;
  followers: number;
}

const UserProfile = () => {
  const { data, isFetching } = useGetFavoriteGithubUsers();
  const renderBodyTable = useMemo(() => {
    if (!data?.length) {
      return (
        <tr>
          <td colSpan={6} style={{ textAlign: 'center' }}>
            No Data Available
          </td>
        </tr>
      );
    }
    return data?.map((item: Data) => (
      <tr key={item.id}>
        <td>{item.id}</td>
        <td>
          <div className={styles.loginSection}>
            <FallbackImage
              className={styles.profileIcon}
              src={item.avatar_url}
            />
            {item.login}
          </div>
        </td>
        <td>
          <div className={styles.wrapText}>
            <a href={item.html_url} rel='noreferrer' target='_blank'>
              {item.html_url}
            </a>
          </div>
        </td>
        <td>{item.public_repos}</td>
        <td>{item.followers}</td>
      </tr>
    ));
  }, [data]);
  const phoneNumber = useSelector(getPhoneNumber);
  return (
    <div className={styles.container}>
      <WarningAPIExceeded />
      <div>User Profile</div>
      <div>Phone: {phoneNumber} </div>
      <div>List of Favorite Github User</div>
      <table className={styles.table}>
        <thead>
          <tr>
            <th>ID</th>
            <th>Login</th>
            <th>HTML URL</th>
            <th>Public Repos</th>
            <th>Followers</th>
          </tr>
        </thead>
        <tbody>{renderBodyTable}</tbody>
      </table>
    </div>
  );
};

export default UserProfile;
