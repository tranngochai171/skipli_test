import Loader from '../../components/common/loader/Loader';
import { useGetGithubUsers } from '../../hooks/useGithub';
import styles from './Users.module.css';
import { FaUser } from 'react-icons/fa';
import Dropdown, {
  OptionDropdownType,
} from '../../components/common/dropdown/Dropdown';
import commonConstants from '../../constants/common.constant';
import Pagination from '../../components/common/pagination/Pagination';
import GithubDashboard from '../../components/githubDashboard/GithubDashboard';
import { useNavigate } from 'react-router-dom';
import WarningAPIExceeded from '../../components/common/warningAPIExceeded/WarningAPIExceeded';

const Users = () => {
  const navigate = useNavigate();
  const {
    keyword,
    onHandleChangeKeyword,
    handleChangePerPage,
    page,
    totalPage,
    handleChangePage,
    query: { isFetching, data },
  } = useGetGithubUsers();
  return (
    <div className={styles.container}>
      <WarningAPIExceeded />
      <div className={styles.header}>
        <input
          type='text'
          placeholder='Search...'
          defaultValue={keyword}
          onChange={onHandleChangeKeyword}
          className={styles.searchBar}
        />
        <FaUser
          size='30'
          style={{ cursor: 'pointer' }}
          color='#1e90ff'
          onClick={() => navigate('/user-profile')}
        />
        <Dropdown
          onChange={(option: OptionDropdownType) =>
            handleChangePerPage(option.value as number)
          }
          options={commonConstants.PER_PAGE_OPTIONS}
        />
        {isFetching ? <Loader /> : null}
      </div>
      <GithubDashboard data={data?.items ?? []} />
      <Pagination
        page={page}
        handleChangePage={handleChangePage}
        totalPages={totalPage}
      />
    </div>
  );
};

export default Users;
