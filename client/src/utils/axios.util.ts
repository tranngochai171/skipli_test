import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BACK_END_API;

export default axios.create({
  baseURL: BASE_URL,
  headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
  withCredentials: true,
});
