export const GITHUB_USERS = 'GITHUB_USERS';
export const FAVORITE_GITHUB_USERS = 'FAVORITE_GITHUB_USERS';

const getQueryKey = {
  getGithubUsersQueryKey: (
    page: number,
    keyword: string,
    perPage: number,
  ): any[] => [GITHUB_USERS, page, keyword, perPage],
  getFavoriteGithubUsersQueryKey: (phoneNumber: string): any[] => [
    FAVORITE_GITHUB_USERS,
    phoneNumber,
  ],
};

export default getQueryKey;
