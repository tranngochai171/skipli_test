import commonConstants from '../constants/common.constant';

const commonUtil = {
  parseTotalPage: ({
    total,
    page,
    perPage,
  }: {
    page: number;
    perPage: number;
    total: number;
  }) => {
    let tempTotalPage = page;
    if (!!total && total >= perPage) {
      tempTotalPage = Math.ceil(total / perPage);
    }
    return tempTotalPage;
  },
  findPerPageOption: (perPage: number) =>
    commonConstants.PER_PAGE_OPTIONS.find(({ value }) => value === perPage) ??
    commonConstants.PER_PAGE_OPTIONS[0],
};

export default commonUtil;
