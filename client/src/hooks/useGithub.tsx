import { useCallback, useMemo, useEffect } from 'react';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useDispatch, useSelector } from 'react-redux';
import { getGithubUsersFilterProperties } from '../redux/selectors/githubUsers.selector';
import getQueryKey, { FAVORITE_GITHUB_USERS } from '../utils/get-query-key';
import axiosUtil from '../utils/axios.util';
import commonConstants from '../constants/common.constant';
import commonUtil from '../utils/common.util';
import {
  PROPERTY_KEY,
  setProperties,
  setProperty,
} from '../redux/slices/githubUsersSlice';
import { useDebounce } from './useDebounce';
import {
  getAuthProperties,
  getPhoneNumber,
} from '../redux/selectors/auth.selector';
import { toast } from 'react-toastify';
import { getUserDetail } from '../redux/slices/authSlice';
import { ThunkDispatch } from '@reduxjs/toolkit';

type GetGithubUsersAPIType = {
  page: number;
  perPage: number;
  keyword: string;
};

type UpdateFavoriteGithubUsersAPIType = {
  phoneNumber: string;
  githubId: number;
};

const getGithubUsersAPI = ({
  page,
  perPage,
  keyword,
}: GetGithubUsersAPIType) => {
  return axiosUtil.get(
    `/github/users?page=${page}&perPage=${perPage}&q=${keyword}`,
  );
};

const updateFavoriteGithubUsersAPI = ({
  phoneNumber,
  githubId,
}: UpdateFavoriteGithubUsersAPIType) => {
  return axiosUtil.post(`/github/update-favorite-github-users`, {
    phoneNumber,
    githubId,
  });
};

export const useGetGithubUsers = () => {
  const dispatch = useDispatch();
  const queryClient = useQueryClient();
  const { keyword, perPage, page } = useSelector(
    getGithubUsersFilterProperties,
  );
  const returnQueryParams = useCallback(
    (isPrefetch: boolean = false) => {
      return {
        page: isPrefetch ? page + 1 : page,
        perPage,
        keyword,
      };
    },
    [page, perPage, keyword],
  );

  const handleChangePage = (newPage: number) => {
    dispatch(setProperty({ type: PROPERTY_KEY.PAGE, value: newPage }));
  };

  const handleChangePerPage = (newPerPage: number) => {
    dispatch(
      setProperties([
        { type: PROPERTY_KEY.PER_PAGE, value: newPerPage },
        {
          type: PROPERTY_KEY.PAGE,
          value: commonConstants.GITHUB_USERS_DEFAULT_QUERY_PARAMS.page,
        },
      ]),
    );
  };

  const handleDebounceFn = useCallback(
    (inputValue: string) => {
      dispatch(
        setProperties([
          { type: PROPERTY_KEY.KEYWORD, value: inputValue },
          {
            type: PROPERTY_KEY.PAGE,
            value: commonConstants.GITHUB_USERS_DEFAULT_QUERY_PARAMS.page,
          },
        ]),
      );
    },
    [dispatch],
  );
  const handleChangeInput = useDebounce(handleDebounceFn);
  const onHandleChangeKeyword = (e: React.ChangeEvent<HTMLInputElement>) => {
    handleChangeInput(e.target.value);
  };

  const query = useQuery(
    getQueryKey.getGithubUsersQueryKey(page, keyword, perPage),
    () => getGithubUsersAPI(returnQueryParams()),
    {
      select: response => response?.data,
      keepPreviousData: true,
      staleTime: commonConstants.STALE_TIME.MIN_1, // 1m },
    },
  );

  const totalPage = useMemo(() => {
    return commonUtil.parseTotalPage({
      total: query?.data?.total_count,
      page: commonConstants.GITHUB_USERS_DEFAULT_QUERY_PARAMS.page,
      perPage,
    });
  }, [perPage, query?.data]);

  useEffect(() => {
    if (page <= totalPage - 1) {
      queryClient.prefetchQuery(
        getQueryKey.getGithubUsersQueryKey(page + 1, keyword, perPage),
        () => getGithubUsersAPI(returnQueryParams(true /* isPrefetch */)),
      );
    }
  }, [page, keyword, queryClient, returnQueryParams, totalPage, perPage]);

  return {
    perPage,
    page,
    keyword,
    query,
    totalPage,
    handleChangePage,
    onHandleChangeKeyword,
    handleChangePerPage,
  };
};

export const useUpdateFavoriteGithubUsers = () => {
  const phoneNumber = useSelector(getPhoneNumber);
  const dispatch = useDispatch<ThunkDispatch<any, any, any>>();
  const queryClient = useQueryClient();
  return useMutation(
    ({ githubId }: Pick<UpdateFavoriteGithubUsersAPIType, 'githubId'>) =>
      updateFavoriteGithubUsersAPI({ phoneNumber, githubId }),
    {
      onSuccess: () => {
        toast.success('Updated Successfully!');
        dispatch(getUserDetail(phoneNumber));
        queryClient.invalidateQueries([FAVORITE_GITHUB_USERS]);
      },
    },
  );
};

type GetGithubUserAPIType = {
  githubId: number;
};

const getGithubUserAPI = ({ githubId }: GetGithubUserAPIType) => {
  return axiosUtil.get(`/github/user?githubId=${encodeURIComponent(githubId)}`);
};

export const useGetFavoriteGithubUsers = () => {
  const { favoriteGithubUsers, phoneNumber } = useSelector(getAuthProperties);
  return useQuery(
    getQueryKey.getFavoriteGithubUsersQueryKey(phoneNumber),
    async () => {
      const listPromise = favoriteGithubUsers.map((githubId: number) =>
        getGithubUserAPI({ githubId }),
      );
      const response = await Promise.allSettled(listPromise);
      console.log('response', response);
      const fulfilledResponse = response
        .filter(item => item?.status === 'fulfilled')
        // @ts-ignore
        .map(item => item?.value?.data);
      return fulfilledResponse || [];
    },
  );
};
