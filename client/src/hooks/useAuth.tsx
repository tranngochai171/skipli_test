import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { login, logout } from '../redux/slices/authSlice';
import axiosUtil from '../utils/axios.util';

type LoginPayload = {
  accessCode: string;
  phoneNumber: string;
};

export const useSendSMS = () => {
  return useMutation(
    ({ phoneNumber }: Pick<LoginPayload, 'phoneNumber'>) =>
      axiosUtil.post('/auth/sendSMS', {
        phoneNumber,
      }),
    {
      onSuccess: (_, { phoneNumber }) => {
        toast.success(`Successfully sent code to ${phoneNumber}`);
      },
    },
  );
};

export const useLogin = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  return useMutation(
    ({ accessCode, phoneNumber }: LoginPayload) =>
      axiosUtil.post('/auth/login', {
        accessCode,
        phoneNumber,
      }),
    {
      onSuccess: (response, { phoneNumber }) => {
        toast.success('Login Successfully');
        dispatch(
          login({
            phoneNumber: response?.data?.phoneNumber,
            favoriteGithubUsers: response?.data?.favoriteGithubUsers ?? [],
          }),
        );
        navigate('/users');
      },
    },
  );
};

export const useLogout = () => {
  const dispatch = useDispatch();
  const queryClient = useQueryClient();
  return useMutation(() => axiosUtil.post('/auth/logout'), {
    onSuccess: () => {
      toast.success('Logout Successfully');
      dispatch(logout());
      queryClient.clear();
    },
  });
};
