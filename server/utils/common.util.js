function generateRandomNumber(length = 6) {
  let num = Math.floor(
    Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1),
  );
  return num;
}

function createSearchParams(obj) {
  const entries = Object.entries(obj).map(([key, value]) => {
    if (value === '') {
      return [key, '""'];
    }
    return [key, value];
  });
  return new URLSearchParams(entries).toString();
}

const getDataFromRef = async ref => {
  const snapshot = await ref.once('value');
  const data = snapshot.val();
  return data;
};

module.exports = { generateRandomNumber, createSearchParams, getDataFromRef };
