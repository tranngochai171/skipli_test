const { GITHUB_URL } = process.env;

const axios = require('axios').default;
class RocketCartAPIClient {
  get(urlSuffix, config = {}) {
    return axios.get(GITHUB_URL + urlSuffix, config);
  }

  post(urlSuffix, data = {}, config = {}) {
    return axios.post(GITHUB_URL + urlSuffix, data, config);
  }

  put(urlSuffix, data = {}, config = {}) {
    return axios.put(GITHUB_URL + urlSuffix, data, config);
  }

  delete(urlSuffix, data = {}, config = {}) {
    return axios.delete(GITHUB_URL + urlSuffix, data, config);
  }
}

module.exports = new RocketCartAPIClient();
