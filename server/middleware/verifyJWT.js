const jwt = require('jsonwebtoken');
const { commonConstants } = require('../constants/common.constant');

const verifyJWT = (req, res, next) => {
  const token = req?.cookies.jwt;

  jwt.verify(token, process.env.JWT_SECRET_KEY, (err, decoded) => {
    if (err) return res.sendStatus(403); // invalid token
    next();
  });
};

module.exports = verifyJWT;
