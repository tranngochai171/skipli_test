var admin = require('firebase-admin');
var credentials = require('./key.json');

admin.initializeApp({
  credential: admin.credential.cert(credentials),
  databaseURL:
    'https://skiplitest-default-rtdb.asia-southeast1.firebasedatabase.app',
});

var db = admin.firestore();

module.exports = { db, admin };
