const jwt = require('jsonwebtoken');
const { commonConstants } = require('../constants/common.constant');
const {
  generateRandomNumber,
  getDataFromRef,
} = require('../utils/common.util');
const twilio = require('twilio');
const { getUserRef } = require('../services/user.service');
const { TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN, TWILIO_SERVICE_SID } =
  process.env;
const client = twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
const FROM_NUMBER = '+19016574342';

var sendSMS = async (req, res) => {
  try {
    const { phoneNumber } = req.body;
    if (!phoneNumber) {
      return res.status(400).json({ message: 'Phone Number is required.' });
    }
    const accessCode = generateRandomNumber();

    const usersRef = getUserRef();
    const usersSnapshot = await usersRef.once('value');

    let userFound = false;
    let userRef;
    usersSnapshot.forEach(user => {
      if (user.val().phoneNumber === phoneNumber) {
        userFound = true;
        userRef = usersRef.child(user.key);
      }
    });
    //  check phone if exist in database
    //  if not create a new one
    if (!userFound) {
      userRef = usersRef.push();
      await userRef.set({
        phoneNumber: phoneNumber,
        accessCode: accessCode,
        favoriteGithubUsers: [],
      });
      console.log(`New user created with phone number ${phoneNumber}`);
    } else {
      await userRef.update({
        accessCode: accessCode,
      });
      console.log(
        `Access code updated for user with phone number ${phoneNumber}`,
      );
    }
    // Since I am using free version Then it has limit on the free balance account
    // Uncomment this code for real sms text
    // client.messages
    //   .create({
    //     to: phoneNumber,
    //     body: 'Skipli Test, Your code: ' + accessCode,
    //     from: FROM_NUMBER,
    //     messagingServiceSid: TWILIO_SERVICE_SID,
    //   })
    //   .then(message => {
    //     console.log('message', message);
    //     return res.status(200).send({ accessCode });
    //   })
    //   .done();
    return res.status(200).send({ accessCode }); // Expose this accessCode for DEV purpose, Will be remove on PROD
  } catch (e) {
    console.log(e);
    if (e.code === 60200) {
      return res.status(500).send({ message: `Invalid phone number!` });
    } else return res.status(500).send(e);
  }
};

var handleLogin = async (req, res) => {
  try {
    const { accessCode, phoneNumber } = req.body;
    if (!accessCode || !phoneNumber) {
      return res
        .status(400)
        .json({ message: 'SMS Code or Phone Number are required.' });
    }

    const usersRef = getUserRef();
    const usersSnapshot = await usersRef.once('value');

    let userFound = false;
    let userRef;

    usersSnapshot.forEach(user => {
      if (
        user.val().phoneNumber === phoneNumber &&
        +user.val().accessCode === +accessCode
      ) {
        userFound = true;
        userRef = usersRef.child(user.key);
      }
    });

    if (!userFound) {
      console.log(`No user found with phone number ${phoneNumber}`);
      return res.status(404).send({ message: 'Invalid Code' });
    }

    // create JWTs
    const accessToken = jwt.sign({ phoneNumber }, process.env.JWT_SECRET_KEY, {
      expiresIn: '1d',
    });

    res.cookie(commonConstants.JWT, accessToken, {
      httpOnly: true,
      secure: true,
      maxAge: 86400000 * 1, // 1 day
    });

    const data = await getDataFromRef(userRef);

    res.status(200).send({
      ...data,
    });
  } catch (e) {
    console.log(e);
    return res.status(500).send(e);
  }
};

var handleLogout = async (req, res) => {
  try {
    res.cookie(commonConstants.JWT, '', {
      httpOnly: true,
      secure: true,
      maxAge: 1, // 1 day
    });

    res.status(201).send();
  } catch (e) {
    return res.status(500).send(e);
  }
};

var getUserDetail = async (req, res) => {
  try {
    const { phoneNumber } = req.query;
    if (!phoneNumber) {
      return res.status(400).json({ message: 'Phone Number is required.' });
    }

    const usersRef = getUserRef();
    const usersSnapshot = await usersRef.once('value');

    let userFound = false;
    let userRef = null;
    usersSnapshot.forEach(user => {
      if (user.val().phoneNumber === phoneNumber) {
        userFound = true;
        userRef = usersRef.child(user.key);
      }
    });
    if (!userFound) {
      console.log(`No user found with phone number ${phoneNumber}`);
      return res
        .status(404)
        .send({ message: `No user found with phone number ${phoneNumber}` });
    }

    const data = await getDataFromRef(userRef);
    res.status(200).send({ ...data });
  } catch (e) {
    console.log(e);
    return res.status(500).send(e);
  }
};

module.exports = { handleLogin, handleLogout, sendSMS, getUserDetail };
