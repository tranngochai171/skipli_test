const { getUserRef } = require('../services/user.service.js');
const {
  createSearchParams,
  getDataFromRef,
} = require('../utils/common.util.js');
const githubAPIClient = require('../utils/github-api.util.js');

var searchGithubUsers = async (req, res) => {
  try {
    const { q = '', page = 1, perPage = 20 } = req.query;
    const searchParams = createSearchParams({
      q,
      page,
      per_page: perPage,
    });
    const URI = '/search/users?' + searchParams.toString();
    const response = await githubAPIClient.get(URI);
    res.status(200).send(response?.data);
  } catch (e) {
    console.log(e);
    return res.status(500).send(e);
  }
};

var updateFavoriteGithubUsers = async (req, res) => {
  try {
    const { phoneNumber, githubId } = req.body;
    if (!phoneNumber || !githubId) {
      return res.status(400).json({
        message: 'Missing required fields: phoneNumber or githubId',
      });
    }
    const usersRef = getUserRef();
    const usersSnapshot = await usersRef.once('value');
    let userFound = false;
    let userRef;
    usersSnapshot.forEach(user => {
      if (user.val().phoneNumber === phoneNumber) {
        userFound = true;
        userRef = usersRef.child(user.key);
      }
    });
    if (!userFound) {
      console.log(`No user found with phone number ${phoneNumber}`);
      return res
        .status(404)
        .send({ message: `No user found with phone number ${phoneNumber}` });
    }

    const userData = await getDataFromRef(userRef);
    console.log('userData', userData);

    let favoriteGithubUsers = userData?.favoriteGithubUsers || [];
    if (!favoriteGithubUsers.includes(githubId)) {
      console.log('ADDED!');
      favoriteGithubUsers.push(githubId);
    } else {
      console.log('REMOVED');
      favoriteGithubUsers = favoriteGithubUsers.filter(
        item => item !== githubId,
      );
    }

    await userRef.update({
      favoriteGithubUsers,
    });
    return res.sendStatus(201);
  } catch (e) {
    console.log(e);
    return res.status(500).send(e);
  }
};

var getGithubUSerProfile = async (req, res) => {
  try {
    const { githubId } = req.query;
    if (!githubId) {
      return res.status(400).json({
        message: 'Missing required fields: githubId',
      });
    }
    const URI = '/user/' + githubId;
    const response = await githubAPIClient.get(URI);
    res.status(200).send(response?.data);
  } catch (e) {
    console.log(e);
    return res.status(500).send(e);
  }
};

module.exports = {
  searchGithubUsers,
  updateFavoriteGithubUsers,
  getGithubUSerProfile,
};
