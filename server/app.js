var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var errorHandler = require('./middleware/errorHandler');
var { logger } = require('./middleware/logEvents');
var corsOptions = require('./config/corsOptions');
var cors = require('cors');

var app = express();

// custom middleware logger
app.use(logger);
// built-in middleware for json

// Cross Origin Resource Sharing
app.use(cors(corsOptions));

app.use(express.json());
// built-in middleware to handle urlencoded form data
app.use(express.urlencoded({ extended: false }));
// middleware for cookies
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/auth', require('./routes/auth'));
app.use('/github', require('./routes/github'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// handle error
app.use(errorHandler);

module.exports = app;
