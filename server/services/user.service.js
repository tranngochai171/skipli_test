const { admin } = require('../db/db');

var getUserRef = () => {
  const database = admin.database();
  const ref = database.ref('/users');
  return ref;
};

module.exports = { getUserRef };
