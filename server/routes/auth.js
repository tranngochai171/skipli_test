var express = require('express');
var router = express.Router();
var authController = require('../controllers/auth.controller');
const verifyJWT = require('../middleware/verifyJWT');

router.post('/sendSMS', authController.sendSMS);
router.post('/login', authController.handleLogin);
router.post('/logout', authController.handleLogout);
router.get('/get-detail', verifyJWT, authController.getUserDetail);

module.exports = router;
