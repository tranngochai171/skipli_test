var express = require('express');
var githubController = require('../controllers/github.controller');
var router = express.Router();
var verifyJWT = require('../middleware/verifyJWT');

router.use(verifyJWT);

router.get('/users', githubController.searchGithubUsers);
router.get('/user', githubController.getGithubUSerProfile);
router.post(
  '/update-favorite-github-users',
  githubController.updateFavoriteGithubUsers,
);

module.exports = router;
