# SKIPLI_TEST Twili Phone Verify App and Github Favorites App
This project was built with client - server structure by [Hai Tran](https://gitlab.com/tranngochai171). To run this app you need to install NodeJS and NPM

## Client & Setup (folder client)
Made with [Create React App](https://github.com/facebook/create-react-app).

### Setup environment variables
Using .env already existed in the Repo.

### Scripts
In the project directory, you can run:

#### `npm install`
This command installs a package and any packages that it depends on.

#### `npm run start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### Technologies:
This is a web application built using ReactJS for front-end and ExpressJS for back-end and Firebase for data storage. Utilizing the following technologies and libraries:

##### ReactJS
ReactJS is a JavaScript library for building user interfaces. It's a popular choice for building dynamic and reactive web applications due to its ability to efficiently render and update components.

##### ExpressJS
ExpressJS is a back-end web application framework built on top of NodeJS. It provides a simple and flexible way to handle HTTP requests and responses, making it a popular choice for building RESTful APIs.

##### Firebase
Firebase is a Backend-as-a-Service (BaaS) platform that provides a variety of tools and services for developing web and mobile applications. In this application, Firebase is used for data storage and management.

##### Styling: CSS Modules
CSS Modules is a CSS file in which all class names and animation names are scoped locally by default. This makes it easier to maintain and avoids naming conflicts in the code.

##### React Router DOM
React Router DOM is a routing library for React applications. It allows for defining and handling routes in a React app, allowing for different pages and views to be displayed based on the current URL.

##### Redux Toolkit
Redux Toolkit is a set of tools and conventions for writing Redux logic. It provides a simple and efficient way to manage state in React applications.

##### Query / Mutate Data: react-query
React-query is a library for fetching, caching, and updating asynchronous data in React applications. It makes it easy to fetch data from APIs and keep your UI in sync with the latest data.

##### Form Validation: Formik and Yup
Formik is a library for handling forms in React, while Yup is a library for object schema validation. These libraries are used together to handle form validation in the application.

##### HTTP Requests: Axios
Axios is a library for making HTTP requests in JavaScript. It provides a simple API for sending and receiving data from APIs.

##### Authentication: JWT and Cookies
JWT (JSON Web Tokens) is a standard for securely transmitting information between parties. In this application, JWT is used for authentication purposes, while cookies are used to store the JWT on the client-side.

## Server & Setup (folder server)
Made with ExpressJS.

### Setup environment variables
Using .env already existed in the Repo.

### Scripts
In the project directory, you can run:

#### `npm install`
This command installs a package and any packages that it depends on.

#### `npm run dev`
Runs the app in the development mode.\
Server will start on Host [http://localhost:3001](http://localhost:3001).

## How it works
You need to start both client side and server side to start to this project

### Step 1: Send an SMS (with Access Code) through Phone number
![send_sms](https://user-images.githubusercontent.com/38663206/216802245-26ab2611-e351-4b0b-beb8-57f9f3e5bdb5.gif)

### Step 2: Verify Access Code and Login
![firebase_access_code](https://user-images.githubusercontent.com/38663206/216802471-7184db7e-c98c-4226-a883-7183ee7b0ae1.png)
![verify_code](https://user-images.githubusercontent.com/38663206/216802451-9313781d-3589-4130-b806-b0d330ad3ea6.gif)

**If you don't receive the Access Code via SMS, you can locate it in the response of the sendSMS request.**

![warning](https://user-images.githubusercontent.com/111322505/216814653-b344b514-121a-4641-baf1-b58b2d959125.png)

### Step 3: Search Github Users
![search](https://user-images.githubusercontent.com/38663206/216802595-c725afe5-e82d-4863-8f61-852ba80ea055.gif)

### Step 4: Change page and Per Page
![change_page_and_per_page_3](https://user-images.githubusercontent.com/38663206/216803117-f275554c-ef36-48cc-8994-09ee00fb5cbc.gif)

### Step 5: Favorite and Unfavorite Github Users Functionality
![favorite_unfavorite_github_users_2](https://user-images.githubusercontent.com/38663206/216802937-05a9dd9b-2968-4350-99a0-7f36137e909b.gif)
![firebase_favorite_users](https://user-images.githubusercontent.com/38663206/216803010-2a99ca05-f545-4076-a9ed-1e44a03118f9.png)

### Step 6: Logout Functionality
![logout_functionality](https://user-images.githubusercontent.com/38663206/216803140-b6f77f6c-a0d9-4308-a304-f744429658c2.gif)

## Contact

For further information, please contact:
### Email: tranngochai171@gmail.com
[LinkedIn](https://www.linkedin.com/in/topytran/).
[Gitlab](https://gitlab.com/tranngochai171).
[Github](https://github.com/tranngochai171).
